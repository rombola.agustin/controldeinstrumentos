# Antes de correrlo por primera vez: 
# pip3 install pyvisa-py pyvisa numpy PyUSB

import pyvisa
rm = visa.ResourceManager()
osci = rm.open_resource('USB0::0x0699::0x0368::C033910::INSTR')
osciName = osci.query('*IDN?')
print(f'Oscilloscope {osciName} is open')

osci.write('MEASUrement:MEAS1:TYPE PK2pk')
osci.write('MEASUrement:MEAS1:SOUrce1')
osci.write('MEASUrement:MEAS2:TYPE PK2pk')
osci.write('MEASUrement:MEAS2:SOUrce2')
osci.write('MEASUrement:MEAS2:TYPE PK2pk')
osci.write('MEASUrement:MEAS3:SOUrce1')
osci.write('MEASUrement:MEAS3:TYPE PHAse')
osci.write('MEASUrement:MEAS3:SOUrce CH2')
osci.write('MEASUrement:MEAS3:TYPE PHAse')
print(osci.query('MEASUrement:MEAS1?'))
print(osci.query('MEASUrement:MEAS1:VAL?'))
print(osci.query('MEASUrement:MEAS2?'))
print(osci.query('MEASUrement:MEAS2:VAL?'))
print(osci.query('MEASUrement:MEAS3?'))


osci.close()