import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import time
from scipy.optimize import curve_fit

fileNames = ['40pf','50pf','60pf','70pf','80pf','90pf','100pf']
rootFile = 'mediciones/'
data = {}
for i in fileNames:
	data[i] = pd.read_csv(rootFile+i+'.csv')
	data[i].Frequency = data[i].Frequency/1e6

print(data['40pf'].head())

plt.plot(data['40pf'].Frequency,data['40pf'].Voltages*5)
plt.plot(data['40pf'].Frequency,data['40pf'].Impedance*20)
plt.plot(data['40pf'].Frequency,data['40pf'].Phase)
plt.show()