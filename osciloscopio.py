#CODIGO ESCRITO PARA UTILIZAR EN EL OSCILOSCOPIO TEKTONIX TBS 1052B
# Antes de correrlo por primera vez: 
# pip3 install pyvisa-py pyvisa numpy PyUSB
# If you run this on Linux, you must use "sudo su" for using it as superuser
import pyvisa as visa
import numpy as np
import time
#Abre la comunicación con el osciloscopio
def open():
    # Cargamos el Resource Manager. El manejador de recursos VISA
    rm = visa.ResourceManager()
    # Muestra cuales son tus dispositivos conectados
    print(f'Availables devices:{rm.list_resources()}')
    try:
        # Informamos la dirección de acceso al osciloscopio (en este caso, por USB)
        #osci = rm.open_resource('USB0::0x0699::0x0368::C033910::INSTR') #Compu de fede
        osci = rm.open_resource('USB0::1689::872::C033910::0::INSTR') #Compu de agus       
        # Chequeo el nombre del instrumento
        osciName = osci.query('*IDN?')
        print(f'Oscilloscope {osciName} is open')
    except:
        print('ERROR: Communication is failed.')
        osci = 0
    return osci

#Setea el trigger 
def setTrigger(osci):
    osci.write('TRIG:MAIN:LEVEL 0')

#Setea modo de adquisición SAM | PEAK | AVE}
def setSample(osci,mode):
    osci.write("ACQ:MOD "+str(mode))
    if str(mode) == 'AVE':
        osci.write('ACQuire:NUMAVg 16') #16 es lo optimo
#Setea que el tipo de datos que se quiere medir
def setMesType(osci):
    osci.write('MEASUrement:MEAS1:TYPE PK2pk')
    osci.write('MEASUrement:MEAS1:SOUrce1')
    osci.write('MEASUrement:MEAS2:TYPE PK2pk')
    osci.write('MEASUrement:MEAS2:SOUrce2')
    osci.write('MEASUrement:MEAS2:TYPE PK2pk')
    osci.write('MEASUrement:MEAS3:SOUrce1')
    osci.write('MEASUrement:MEAS3:TYPE PHAse')
    
#Setea la escala
def setScale(osci,scale,channel):
    osci.write('CH'+str(channel)+':VOLTS '+str(scale))
    
#Confirma que sea la escala adecuada y si no es la cambia 

def checkScale(osci,timeSleep,channel):
    volt_scales = np.array([2e-3,5e-3,10e-3,20e-3,50e-3,100e-3,0.2,0.5,1,2,5])
    
    value = osci.query_ascii_values('MEASUrement:MEAS'+str(channel)+':VAL?')[0]
    scale = osci.query_ascii_values('CH'+str(channel)+':VOLTS?')[0]
    
    while(value > 7*scale):
        print('The scale is too small')
        indexValue = np.where(volt_scales == scale)[0][0] #Busca el indice correspondiente al valor de escala
        osci.write('CH'+str(channel)+':VOLTS '+ str(volt_scales[indexValue+1]))
        print(f'Old scale: {volt_scales[indexValue]} ---> New scale: {volt_scales[indexValue+1]}')
        scale = volt_scales[indexValue+1] #Setea como escala la nueva para volver a comparar
        time.sleep(timeSleep)
        value = osci.query_ascii_values('MEASUrement:MEAS'+str(channel)+':VAL?')[0]

    while(value < 3*scale):
        print('The scale is too big')
        indexValue = np.where(volt_scales == scale)[0][0] #Busca el indice correspondiente al valor de escala
        osci.write('CH'+str(channel)+':VOLTS '+ str(volt_scales[indexValue-1]))
        print(f'Old scale: {volt_scales[indexValue]} ---> New scale: {volt_scales[indexValue-1]}')
        scale = volt_scales[indexValue-1] #Setea como escala la nueva para volver a comparar
        time.sleep(timeSleep)
        value = osci.query_ascii_values('MEASUrement:MEAS'+str(channel)+':VAL?')[0]

def getValue(osci):
    v1 = osci.query_ascii_values('MEASUrement:MEAS1:VAL?')
    v2 = osci.query_ascii_values('MEASUrement:MEAS2:VAL?')
    v3 = osci.query_ascii_values('MEASUrement:MEAS3:VAL?')
    return v1,v2,v3
        
#Cierra la comunicación
def close(osci):
    osci.close()
    print('Communication with oscilloscope is closed.')
