# ------------  MAIN ----------------

import generador as gen
import osciloscopio as osc
import numpy as np
import time
import pandas as pd
import matplotlib.pyplot as plt
#%% Seteo de variables principales
startSweep = 5.3e6        # MHz
stepSweep = 2e3           # KHz
stopSweep = 5.7e6         # MHz
voltage = 5             # Vpp
amplification = 114     # Amplificacion del cable
arraySweep = np.arange(startSweep,stopSweep+stepSweep,stepSweep)

#%% Seteo colores para formatear la consola
colorWhite = '\u001b[37m'
colorYellow = '\u001b[33m'
colorRed = '\u001b[31m'
colorCyan = '\u001b[36m'
colorGreen = '\u001b[32m'

#%% Programa


# Abre la comunicación con el generador y el osciloscopio
g = gen.open()
o = osc.open()

# Setea el voltage y la escala del generador
gen.setVoltage(g,voltage)

# Setea las config iniciales de osciloscopio
osc.setTrigger(o)
osc.setMesType(o)
#osc.setSample(o,'SAM') #Sample
osc.setSample(o,'AVE') #Para promediar
 
voltagesPeak = []
impedance = []
phase = []
# Barrido en frecuencias dentro del generador y para medir
p = 0  # For progress bar
for i in arraySweep:
    p += 1
    
    gen.setFreq(g,i)
    
    trySecurity = True
    
    while trySecurity:
        print(f'{colorCyan}Checking scale for channel 1 ...{colorWhite}')
        try:
            osc.checkScale(o,3,1)   #sleep to change scale, channel
            trySecurity = False
        except:
            trySecurity = True
    
    #time.sleep(4)

    trySecurity = True
    while trySecurity:
        print(f'{colorCyan}Checking scale for channel 2 ...{colorWhite}')
        try:
            osc.checkScale(o,3,2)  #sleep to change scale, channel
            trySecurity = False
        except:
            trySecurity = True
    
    time.sleep(4)

    trySecurity = True
    while trySecurity:
        print(f'{colorYellow}Getting data ...{colorWhite}')
        try: 
            va,im,ph = osc.getValue(o)
            trySecurity = False
        except:
            trySecurity = True
    
    voltagesPeak.append(va[0])
    impedance.append(im[0])
    phase.append(ph[0])
    #print(voltagesPeak[-1])
    print(f'{colorYellow} Progress ... {round((p/len(arraySweep))*100,2)}% {colorWhite}')
# Cierre de conexiones
gen.close(g)
osc.close(o)

#%% 
#Guarda los archivos de las mediciones
fileNameInput = input('Please, write a filename: ')
fileName = 'mediciones/'+str(fileNameInput)+'.csv'
voltagesPeak = np.array(voltagesPeak)*(amplification/voltage)
voltagesPeak = pd.Series(voltagesPeak)
arraySweep = pd.Series(arraySweep)
impedance = pd.Series(impedance)
phase = pd.Series(phase)

data = {'Frequency':arraySweep,'Voltages':voltagesPeak,'Impedance':impedance,'Phase':phase}
data = pd.DataFrame(data)
data.to_csv(fileName)
print(f'{colorGreen}Data is saved in csv file{colorWhite}')

#%% 
# Graficador
plt.figure(1)
plt.scatter(arraySweep,voltagesPeak)
plt.savefig('voltagespeak.png')
plt.figure(2)
plt.scatter(arraySweep,impedance)
plt.savefig('impedance.png')
plt.figure(3)
plt.scatter(arraySweep,phase)
plt.savefig('arraysweep.png')
