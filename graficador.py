import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit

def fit_func(omega_, f_, omega0_, gamma_,offset_):
	return f_/(np.sqrt((omega0_**2-omega_**2)**2+4*(gamma_**2)*(omega_**2))) + offset_

def findReflejada(volts):
	indexReflex = np.argmax(volts)
	for i in range(np.argmax(volts),np.argmin(volts)):      #para que busque solo entre max y min
		if abs(volts[i]-0.5) < abs(volts[indexReflex]-0.5):
			indexReflex = i
	return indexReflex

def FixPhase(phase):
	for i in range(len(phase)):
		if (phase[i] > 0):
			phase[i] = -1*phase[i]
	return phase 

# Colors
cPink = '#CA0D39'
cGreen = '#0DCA39'
cBlue = '#2573CB'
cOrange = '#FA851F'

rootMediciones = 'mediciones/'
rootGraficos = 'graficos/'
rootS11 = 'mediciones/s11/capacidad'
vIn = 5
sizeA4 = [8-1/4,11-3/4]
optimalSize = [8,6]

#ERRORES
errorFrec = 0
errorGanancia = 0.3
errorImpedancia = 2
errorFase = 1
errorS11 = 0.2
print('Cargando datos...')
files = np.loadtxt(rootMediciones+'listFiles.txt',dtype=str)

data = {}
s11 = {}
for f in files:
	data[f] = pd.read_csv((rootMediciones+f+'.csv'))
	data[f].Frequency = data[f].Frequency*1e-6
	data[f].Voltages = data[f].Voltages
	data[f].Impedance = data[f].Impedance/vIn    #Divido por vIn para nomalizar
	data[f]['Phase'] = FixPhase(data[f]['Phase'])
	data[f]['ZetaCero'] = (data[f].Impedance * 50)/(1-data[f].Impedance)
	s11[f] = pd.read_csv((rootS11+f+'.csv'),sep=';')
	s11[f]['frec [KHz]'] = s11[f]['frec [KHz]']*1e-3
	s11[f]['S11 [-dB]'] = -1*s11[f]['S11 [-dB]']
'''
numberFigure = 1

#Graficos de Voltage

initialParameters = [5,7,0.1,0.03] #Para eel primer ajuste
print('Graficando los volatajes...')
for f in files:
	numberFigure += 1
	plt.figure(numberFigure,figsize=optimalSize,dpi=280)

	plt.grid(alpha=0.5)
	#plt.errorbar(x_a,y_a,yerr=0.5,xerr=0.004,fmt='.',label='Mediciones')
	plt.errorbar(x=data[f].Frequency,y=data[f].Voltages,yerr=errorGanancia,fmt=',',color=cGreen,label='Mediciones')
	#Ajuste de los datos por una resonancia
	initialParameters[1] = data[f].Frequency[np.argmax(data[f].Voltages)] #Pone como parametro inicial la resonancia 
	xAjuste = np.linspace(np.min(data[f].Frequency)*0.99,np.max(data[f].Frequency)*1.01, 500)
	popt, pcov = curve_fit(fit_func, data[f].Frequency, data[f].Voltages,p0=initialParameters, maxfev=5000)
	initialParameters = popt
	print(popt)
	data[f]['AjusteVoltage'] = fit_func(data[f].Frequency, *popt) #Guardo los puntos ajustados
	q = np.abs(popt[1]/(2*popt[2])) #Quality Factor
	#print(q)
	
	#Plot del ajuste
	plt.plot(xAjuste, fit_func(xAjuste, *popt), '-',color=cPink,alpha=0.8,label='Ajuste')
	freqResonance =  xAjuste[np.argmax(fit_func(xAjuste, *popt))]
	plt.axvline(x=freqResonance)
	plt.title(f'Resonancia para una capacidad {f} , Q = {round(q,2)}')
	plt.legend()
	plt.xlabel('Frecuencia [MHz]')
	plt.ylabel('Ganancia [adim]')
	plt.savefig(rootGraficos+'Cap'+f+'Voltages.png')

#Gráficos de las fases 
print('Graficando las fases...')
for f in files:
	numberFigure += 1
	plt.figure(numberFigure,figsize=optimalSize,dpi=280)
	plt.grid(alpha=0.5)
	errorFase = 1
	plt.errorbar(x=data[f].Frequency,y=data[f].Phase,yerr=errorFase,xerr=errorFrec,fmt=',',label='Mediciones')

	plt.title(f'Diferencia de fase entre la señal de entrada y salida del resonador (C = {f})')
	plt.legend()
	plt.xlabel('Frecuencia [MHz]')
	plt.ylabel('Diferencia de fase [grados]')
	plt.savefig(rootGraficos+'Cap'+f+'Phase.png')


#Gráficos de la reflejada
print('Graficando la reflejada...')
for f in files:
	numberFigure += 1
	plt.figure(numberFigure,figsize=optimalSize,dpi=280)
	plt.grid(alpha=0.5)
	index = (findReflejada(data[f].Impedance))
	plt.errorbar(x=data[f].Frequency,y=data[f]['ZetaCero'],yerr=errorImpedancia,fmt=',',color=cOrange,label='Mediciones')
	bestPoint = str(round(data[f].Frequency[index],3))+' MHz, ' +str(round(data[f]['ZetaCero'][index],3))+' Ohm'
	plt.errorbar(x=data[f].Frequency[index],y=data[f]['ZetaCero'][index],yerr=errorGanancia,fmt='x',color='black',label=bestPoint)
	#print(data[f].Frequency[index],data[f].Impedance[index])
	plt.axvline(x=data[f].Frequency[index],color=cPink,label='Máxima adaptación')
	plt.axhline(y=50,linestyle='--',alpha=0.5,color='gray')
	plt.title(f'Impedancia de entrada del sistema para una capacidad de {f}')
	plt.legend()
	plt.xlabel('Frecuencia [MHz]')
	plt.ylabel('Zin [Ohm]')
	plt.savefig(rootGraficos+'Cap'+f+'Reflejada.png')

#Gráficos de la reflejada juntos con la amplitud
print('Graficando amplitud y reflejada...')

for f in files:
	numberFigure += 1
	plt.figure(numberFigure,figsize=optimalSize,dpi=280)
	plt.errorbar(x=data[f].Frequency,y=data[f].Voltages,yerr=errorGanancia,fmt=',',color=cGreen,label='Mediciones')
	plt.plot(data[f].Frequency,data[f]['AjusteVoltage'], '-',color=cPink,alpha=0.8,label='Ajuste')
	freqResonance = data[f].Frequency[np.argmax(data[f].Voltages)]
	plt.axvline(x=freqResonance,color=cBlue,label='Resonancia')
	plt.legend(loc='upper right')
	plt.ylabel('Ganancia [adim]')
	plt.xlabel('Frecuencia [MHz]')
	plt.xlim([np.min(data[f].Frequency),np.max(data[f].Frequency)])

	#EJE DE LA REFLEJADA
	plt.twinx()

	index = (findReflejada(data[f].Impedance))
	plt.errorbar(x=data[f].Frequency,y=data[f]['ZetaCero'],yerr=errorImpedancia ,fmt=',',color=cOrange,label='Mediciones')
	bestPoint = str(round(data[f].Frequency[index],3))+' MHz, ' +str(round(data[f]['ZetaCero'][index],3))+' Ohm'
	plt.errorbar(x=data[f].Frequency[index],y=data[f]['ZetaCero'][index],yerr=errorImpedancia ,fmt='x',color='black',label=bestPoint)
	plt.axhline(y=50,linestyle='--',alpha=0.5,color='gray')
	#plt.axhline(y=50,xmin=data[f].Frequency[index],xmax=np.max(data[f].Frequency),linestyle='--',alpha=0.5,color='gray')
	#print(data[f].Frequency[index],data[f].Impedance[index])
	#plt.axvline(x=data[f].Frequency[index],color=cBlue,label='Menor reflejada')
	plt.legend(loc='upper left')

	plt.ylabel('Zin [Ohm]')
	if data[f].Frequency[index] > freqResonance:
		plt.title(f'Ganancia vs Reflejado - C = {f} - Rango óptimo [{round(freqResonance,3)},{round(data[f].Frequency[index],3)}] MHz')
	else:
		plt.title(f'Ganancia vs Reflejado - C = {f} - Rango óptimo [{round(data[f].Frequency[index],3)},{round(freqResonance,3)}] MHz')
	plt.savefig(rootGraficos+'Cap'+f+'VoltageAndReflex.png')
'''

#PLOT DEL S11
numberFigure = 1
plt.figure(numberFigure,figsize=[18*0.8,10*0.8],dpi=350)
plt.suptitle('Coeficiente de reflexión (S11) para cada capacidad', fontsize=14,fontweight='bold')
numSubPlot = 0
for f in files:
	numSubPlot += 1
	plt.subplot(3,3,numSubPlot)
	plt.title(f'Capacidad = {f}')
	plt.errorbar(x=s11[f]['frec [KHz]'],y=s11[f]['S11 [-dB]'],yerr=errorS11,fmt=',',color=cBlue,label='Mediciones')
	plt.xlabel('Frecuencia [MHz]')
	plt.ylabel('S11 [dB]')
	plt.ylim([-28,0])
	plt.grid(alpha=0.4)
	indexMin = np.argmin(s11[f]['S11 [-dB]'])
	xBest = s11[f]['frec [KHz]'][indexMin]
	yBest = s11[f]['S11 [-dB]'][indexMin]
	reflex = (10**(yBest/20))*100
	print(f,round(reflex,2))
	best = f'{round(xBest,2)} Mhz,{round(yBest,2)} dB'
	plt.errorbar(x=xBest,y=yBest,yerr=errorS11,fmt='x',color='black',label=best)
	plt.legend(fontsize=7)
	plt.locator_params(tight=True, nbins=5)
plt.tight_layout()
plt.savefig(rootGraficos+'GraficoDelS11.png')

numberFigure +=1
initialParameters = [5,7,0.1,0.03] #Para el primer ajuste
parametrosDeAjuste = {}
#GRAFICOS CON EL SUBPLOT
ncols = 2
nrows = 2
for f in files:
	numberFigure += 1
	#plt.figure(numberFigure,figsize=sizeA4,dpi=500)
	plt.figure(numberFigure,figsize=[18*0.8,10*0.8],dpi=350)
	plt.suptitle(f"Capacidad = {f}", fontsize=16,fontweight='bold')

	#GRAFICO DE GANANCIA
	plt.subplot(nrows,ncols,1)

	plt.grid(alpha=0.4)
	plt.errorbar(x=data[f].Frequency,y=data[f].Voltages,yerr=errorGanancia,fmt=',',color=cGreen,label='Mediciones')
	#Ajuste de los datos por una resonancia
	initialParameters[1] = data[f].Frequency[np.argmax(data[f].Voltages)] #Pone como parametro inicial la resonancia 
	xAjuste = np.linspace(np.min(data[f].Frequency)*0.99,np.max(data[f].Frequency)*1.01, 500)
	popt, pcov = curve_fit(fit_func, data[f].Frequency, data[f].Voltages,p0=initialParameters, maxfev=5000)
	initialParameters = popt #Para usar en el proximo ajuste
	parametrosDeAjuste[f]=popt
	#print(popt)
	data[f]['AjusteVoltage'] = fit_func(data[f].Frequency, *popt) #Guardo los puntos ajustados
	q = np.abs(popt[1]/(2*popt[2])) #Quality Factor
	print(f'Para un {f}, el Q = {q}')
	plt.plot(xAjuste, fit_func(xAjuste, *popt), '-',color=cPink,alpha=0.8,label='Ajuste')

	print(f'Parametros de ajuste (f,omegacero,gamma,offset):')
	for param in range(len(popt)):
		print(popt[param],pcov[param,param])
	freqResonance =  xAjuste[np.argmax(fit_func(xAjuste, *popt))]
	gananciaResonance = np.max(fit_func(xAjuste, *popt))

	plt.axvline(x=freqResonance,color=cBlue)
	plt.text(x=freqResonance+0.01,y=1,s=f'Resonancia {round(freqResonance,3)} MHz',fontsize=7,fontweight='bold',color=cBlue)

	plt.axhline(y=gananciaResonance,linestyle='--',alpha=0.5,color='gray')
	plt.text(x=freqResonance+0.01,y=gananciaResonance-1.5,s=f'Ganancia {round(gananciaResonance,3)}',fontsize=7,fontweight='bold',color='gray')


	plt.title(f'Fig. A', fontsize=12,fontstyle='italic')
	plt.legend(fontsize=8)
	plt.xlim([np.min(data[f].Frequency),np.max(data[f].Frequency)])
	plt.xlabel('Frecuencia [MHz]')
	plt.ylabel('Ganancia [adim]')

	#GRAFICO DE IMPEDANCIA
	plt.subplot(nrows,ncols,2)

	plt.grid(alpha=0.4)
	index = (findReflejada(data[f].Impedance))
	plt.errorbar(x=data[f].Frequency,y=data[f]['ZetaCero'],yerr=errorImpedancia,fmt=',',color=cOrange,label='Mediciones')
	
	bestPoint = str(round(data[f].Frequency[index],3))+' MHz, ' +str(round(data[f]['ZetaCero'][index],3))+' Ohm'
	plt.errorbar(x=data[f].Frequency[index],y=data[f]['ZetaCero'][index],yerr=errorGanancia,fmt='x',color='black')
	plt.text(x=data[f].Frequency[index]+0.005,y=data[f]['ZetaCero'][index],s=bestPoint,fontsize=7,fontweight='bold',color='black')

	plt.axvline(x=data[f].Frequency[index],color=cBlue,label='Máxima adaptación')
	plt.axhline(y=50,linestyle='--',alpha=0.5,color='gray')

	if f == '40pf':
		plt.text(x=6.9,y=55,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')
	if f == '50pf':
		plt.text(x=6.4,y=55,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')
	if f == '60pf':
		plt.text(x=6.1,y=43,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')
	if f == '70pf':
		plt.text(x=5.72,y=40,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')
	if f == '80pf':
		plt.text(x=5.32,y=55,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')
	if f == '90pf':
		plt.text(x=5.01,y=55,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')
	if f == '100pf':
		plt.text(x=4.85,y=55,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')

	plt.title(f'Fig. B', fontsize=12,fontstyle='italic')
	plt.xlim([np.min(data[f].Frequency),np.max(data[f].Frequency)])
	plt.legend(fontsize=8)
	plt.xlabel('Frecuencia [MHz]')
	plt.ylabel('Zin [Ohm]')

	#GRAFICO CONJUNTO DE IMPEDANCIA Y GANANCIA
	plt.subplot(nrows,ncols,3)

	plt.errorbar(x=data[f].Frequency,y=data[f].Voltages,yerr=errorGanancia,fmt=',',color=cGreen,label='Ganancia')
	plt.plot(data[f].Frequency,data[f]['AjusteVoltage'], '-',color=cPink,alpha=0.8,label='Ajuste')
	freqResonance = data[f].Frequency[np.argmax(data[f].Voltages)]
	
	plt.axvline(x=freqResonance,color=cBlue,label='Resonancia')
	plt.text(x=freqResonance+0.01,y=28,s=f'Resonancia {round(freqResonance,3)} MHz',fontsize=7,fontweight='bold',color=cBlue)

	plt.legend(loc='upper right',fontsize=8)
	plt.ylabel('Ganancia [adim]')
	plt.xlabel('Frecuencia [MHz]')
	plt.ylim([0,30])
	plt.xlim([np.min(data[f].Frequency),np.max(data[f].Frequency)])
	#Eje de la impedancia
	plt.twinx()
	index = (findReflejada(data[f].Impedance))
	plt.errorbar(x=data[f].Frequency,y=data[f]['ZetaCero'],yerr=errorImpedancia ,fmt=',',color=cOrange,label='Impedancia')
	bestPoint = str(round(data[f].Frequency[index],3))+' MHz, ' +str(round(data[f]['ZetaCero'][index],3))+' Ohm'

	plt.errorbar(x=data[f].Frequency[index],y=data[f]['ZetaCero'][index],yerr=errorImpedancia ,fmt='x',color='black')
	plt.text(x=data[f].Frequency[index]+0.005,y=data[f]['ZetaCero'][index],s=bestPoint,fontsize=7,fontweight='bold',color='black')

	plt.axhline(y=50,linestyle='--',alpha=0.5,color='gray')
	plt.text(x=np.min(data[f].Frequency)+0.01,y=52,s=f'50 Ohm',fontsize=7,fontweight='bold',color='gray')

	plt.legend(loc='upper left',fontsize=8)
	plt.ylabel('Zin [Ohm]')
	plt.title('Fig. C', fontsize=12,fontstyle='italic')

	#GRAFICO LA FASE
	plt.subplot(nrows,ncols,4)

	plt.grid(alpha=0.4)
	plt.errorbar(x=data[f].Frequency,y=data[f].Phase,yerr=errorFase,fmt=',',label='Mediciones',color=cPink)

	plt.axvline(x=freqResonance,color=cBlue,label='Resonancia')
	plt.text(x=freqResonance+0.01,y=-20,s=f'Resonancia {round(freqResonance,3)} MHz',fontsize=7,fontweight='bold',color=cBlue)

	plt.title(f'Fig. D', fontsize=12,fontstyle='italic')
	plt.legend(fontsize=8)
	plt.ylim([-180,0])
	plt.xlim([np.min(data[f].Frequency),np.max(data[f].Frequency)])
	plt.xlabel('Frecuencia [MHz]',fontsize=10)
	plt.ylabel('Diferencia de fase [grados]',fontsize=10)
	
	#GUARDO EL GRAFICO
	plt.tight_layout()
	plt.savefig(rootGraficos+'Cap'+f+'Cuadruple.png')


numberFigure += 1
print('Grafico de todas las ganancias juntas')
for f in files:
	plt.figure(numberFigure,figsize=optimalSize,dpi=300)
	plt.suptitle(f"Ganancia en función de la frecuencia", fontsize=16,fontweight='bold')
	plt.title("para cada capacidad", fontsize=16,fontweight='bold')
	plt.grid(alpha=0.4)
	p = plt.errorbar(x=data[f].Frequency,y=data[f].Voltages,yerr=errorGanancia,fmt=',',label=f)
	keepColor = p[0].get_color()
	indexMax = np.argmax(data[f].Voltages)
	plt.axvline(x=data[f].Frequency[indexMax],color=keepColor,alpha=0.5)
	plt.errorbar(x=data[f].Frequency[indexMax],y=data[f].Voltages[indexMax],yerr=errorGanancia,fmt='x',color='black')
	plt.legend(fontsize=10,ncol=4,loc='upper center')
	plt.xlabel('Frecuencia [MHz]',fontsize=12)
	plt.ylabel('Ganancia [adim]',fontsize=12)
	plt.ylim([0,35])
plt.savefig(rootGraficos+'TodasLasCapacidadesGanancia.png')

numberFigure += 1
print('Grafico de todas las fases juntas')
for f in files:
	plt.figure(numberFigure,figsize=optimalSize,dpi=300)
	plt.suptitle(f"Diferencia de fase en función de la frecuencia", fontsize=16,fontweight='bold')
	plt.title("para cada capacidad", fontsize=16,fontweight='bold')
	plt.grid(alpha=0.4)
	plt.errorbar(x=data[f].Frequency,y=data[f].Phase,yerr=errorFase,fmt=',',label=f)
	plt.legend(fontsize=10,ncol=4,loc='upper center')
	plt.xlabel('Frecuencia [MHz]',fontsize=12)
	plt.ylabel('Diferencia de fase [grados]',fontsize=12)
	plt.ylim([-180,20])
plt.savefig(rootGraficos+'TodasLasCapacidadesFases.png')

numberFigure += 1
print('Grafico de todas las impedancias juntas')
for f in files:
	plt.figure(numberFigure,figsize=optimalSize,dpi=300)
	plt.suptitle(f"Zin en función de la frecuencia", fontsize=16,fontweight='bold')
	plt.title("para cada capacidad", fontsize=16,fontweight='bold')
	plt.grid(alpha=0.4)
	plt.errorbar(x=data[f].Frequency,y=data[f]['ZetaCero'],yerr=errorImpedancia ,fmt=',',label=f)

	index = (findReflejada(data[f].Impedance))
	plt.errorbar(x=data[f].Frequency[index],y=data[f]['ZetaCero'][index],yerr=errorGanancia,fmt='x',color='black')

	plt.legend(fontsize=10,ncol=4,loc='upper left')
	plt.xlabel('Frecuencia [MHz]',fontsize=12)
	plt.ylabel('Zin [Ohm]',fontsize=12)
plt.axhline(y=50,linestyle='--',alpha=0.5,color='gray')
plt.savefig(rootGraficos+'TodasLasCapacidadesImpedance.png')

# 10**(-s11/20)
# Titulo Capacidad = 40 pf de cada subplot
