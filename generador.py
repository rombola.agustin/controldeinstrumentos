#CODIGO ESCRITO PARA UTILIZAR EN EL GENERADOR RIGOL DG5252
# Antes de correrlo por primera vez: 
# pip3 install pyvisa-py pyvisa numpy PyUSB

import pyvisa as visa

def open():
# Cargamos el Resource Manager. El manejador de recursos VISA
    rm = visa.ResourceManager()
    # Muestra cuales son tus dispositivos conectados
    print(rm.list_resources())
    
    # Informamos la dirección de acceso al osciloscopio (en este caso, por USB)
    #gen = rm.open_resource('USB0::0x1AB1::0x0640::DG5T161850026::INSTR') #compu de fede
    gen = rm.open_resource('USB0::6833::1600::DG5T161850026\x00::0::INSTR') #compu de agus
    genName = gen.query('*IDN?')

    print(f'Generator {genName} is open')
    return gen

def setFreq(gen,freq):
    gen.write(':FREQ '+str(freq))
    print(f'Frequency {freq}')   
    
def setVoltage(gen,volt):
    gen.write(':VOLT:UNIT VPP')
    gen.write(':VOLT '+str(volt))
    
def query(gen):
    a = gen.query(':SOURce1:VOLT?')
    b = gen.query(':SOURce1:VOLT:UNIT?')
    c = gen.query(':FREQ?')
    print(f'Freq {c} // Voltage {a} {b}')
    return a,b,c

def close(gen):
    gen.close()
    print('Communication with generator is closed.')

